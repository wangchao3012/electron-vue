import {
  app,
  BrowserWindow,
  ipcMain,
  screen,
  dialog,
  Menu
} from 'electron'
var os = require('os')
var log = autoUpdater.logger;
/**
 * Set `__static` path to static files in production
 * https://simulatedgreg.gitbooks.io/electron-vue/content/en/using-static-assets.html
 */
if (process.env.NODE_ENV !== 'development') {
  global.__static = require('path').join(__dirname, '/static').replace(/\\/g, '\\\\')
}
let isUpdate = false;
let mainWindow
// 端口
console.log('process.env.Port:::::::::::::::::::::::::::::::::::::::::::::::::', process.env.Port);
const winURL = process.env.NODE_ENV === 'development' ?
  `http://localhost:${process.env.Port}` :
  `file://${__dirname}/index.html`

function createWindow() {
  /**
   * Initial window options 
   */
  const {
    width,
    height
  } = screen.getPrimaryDisplay().workAreaSize;

  var opt = {
    width: width,
    height: height,
    minWidth: 800, // 1280,
    minHeight: 300,
    autoHideMenuBar: true,
    useContentSize: true,
    backgroundColor: '#282d38',
    // transparent: true,
    // frame: false
  }
  if (process.platform == 'darwin') {
    opt.titleBarStyle = 'hiddenInset';
    if (process.env.NODE_ENV === 'production') {
      let template = [{
        label: app.getName(),
        submenu: [{
          label: '隐藏',
          role: 'hide'
        }, {
          label: '隐藏其它',
          role: 'hideothers'
        }, {
          label: '显示所有',
          role: 'unhide'
        }, {
          type: 'separator'
        }, {
          label: '退出',
          role: 'quit',
          click(e) {
            e.preventDefault()
            mainWindow.webContents.send('MainWindowWillClose', true);
          }
        }]
      }, {
        label: '编辑',
        visible: false,
        submenu: [{
            label: '复制',
            role: 'copy'
          },
          {
            label: '粘贴',
            role: 'paste'
          }, {
            label: '全选',
            role: 'selectall'
          }
        ]
      }];
      const menu = Menu.buildFromTemplate(template)
      Menu.setApplicationMenu(menu)
    }

  } else if (process.platform == 'win32') {
    opt.frame = false;
  } else {
    opt.frame = false;
  }

  // set linux icon to the top left of window  
  if (process.platform == 'linux') {
    var exeDir = path.dirname(app.getPath('exe'));
    opt['icon'] = path.join(exeDir, 'icon.png');
  }
  opt.webPreferences = {
    webSecurity: false
  }
  mainWindow = new BrowserWindow(opt);
  setTimeout(() => {
    mainWindow.loadURL(winURL)
  }, 100)

  mainWindow.on('close', (e) => {
    if (process.platform == 'darwin') {
      e.preventDefault();
      if (isUpdate) {
        mainWindow.destroy();
      } else if (mainWindow.isVisible()) {
        mainWindow.webContents.send('MainWindowWillClose', true);
        // app.hide();
      }

    }
  })

  mainWindow.on('closed', () => {

    mainWindow = null
  })


  mainWindow.on('maximize', () => {
    console.log('最大化');

  })
  mainWindow.on('unmaximize', () => {
    console.log('unmaximize');

  })
  mainWindow.on('minimize', () => {
    console.log('minimize');

  })
  mainWindow.on('restore', () => {
    console.log('restore');

  })
  mainWindow.on('enter-full-screen', () => {
    console.log('enter-full-screen');

  })
  mainWindow.on('leave-full-screen', () => {
    console.log('leave-full-screen');

  })

  mainWindow.on('enter-html-full-screen', () => {
    console.log('enter-html-full-screen');

  })
  mainWindow.on('leave-html-full-screen', () => {
    console.log('leave-html-full-screen');

  })

  // mainWindow.flashFrame(true)

  //登录窗口最小化
  ipcMain.on('window-min', function () {
    mainWindow.minimize();
  })
  ipcMain.on('window-dev', function () {
    mainWindow.toggleDevTools();
  })
  //登录窗口最大化
  ipcMain.on('window-max', function (e, d) {
    console.log('mainWindow.isMaximized():::', mainWindow.isMaximized());


    // e.sender.send('create-folder-result', '222');

    if (mainWindow.isMaximized()) {
      // mainWindow.restore();
      console.log('取消最大化');

      mainWindow.unmaximize();
    } else {
      console.log('最大化');
      mainWindow.maximize();
    }
  })

  ipcMain.on('window-close', function () {
    mainWindow.close();
  })

  ipcMain.on('sys.window-close', function () {
    mainWindow.destroy();
  })

  ipcMain.on('sys.update', function () {
    isUpdate = true;
    autoUpdater.quitAndInstall();
  })
  ipcMain.on('sys.checkForUpdates', function () {
    autoUpdater.checkForUpdates()
  })
}

app.on('ready', createWindow)



app.on('window-all-closed', () => {
  // if (process.platform !== 'darwin') {
  app.quit()
  // }
})

app.on('activate', () => {
  if (mainWindow === null) {
    createWindow()
  }
})

/**
 * Auto Updater
 *
 * Uncomment the following code below and install `electron-updater` to
 * support auto updating. Code Signing with a valid certificate is required.
 * https://simulatedgreg.gitbooks.io/electron-vue/content/en/using-electron-builder.html#auto-updating
 */

/**/


import {
  autoUpdater
} from 'electron-updater'
var config = CONFIG;
let network = os.networkInterfaces();
let networkList = [];
for (let k in network) {
  if (network.hasOwnProperty(k)) {
    let it = network[k];
    networkList = networkList.concat(it);
  }
}
networkList = networkList.filter(it => it.mac != '00:00:00:00:00:00')

console.log('networkList:::', networkList);

process.env.networkList = JSON.stringify(networkList);

// autoUpdater.autoDownload = false

// console.log('process.env.updateUrl::',process.env.updateUrl);

// autoUpdater.setFeedURL('http://10.25.24.105:58081/file/');

// debugger
// console.log('process.env::', process.env);


autoUpdater.setFeedURL({
  provider: "generic", // 这里还可以是 github, s3, bintray
  url: config.updateUrl + 'dl/smart/' //'http://10.25.24.106:58081/file/', //
});


autoUpdater.on('error', (error) => {
  log.error('autoUpdater：更新错误');
})

autoUpdater.on('update-available', (info) => {

  mainWindow.webContents.send('MainWindowUpdateAvailable', {
    version: info.version,
    content: info.releaseNotes
  });
})

autoUpdater.on('update-not-available', () => {
  log.info('autoUpdater：没有更新')
})

autoUpdater.on('download-progress', (info) => {

  mainWindow.setProgressBar(info.percent / 100);

  mainWindow.webContents.send('MainWindowDownloadProgress', info.percent);
})
autoUpdater.on('update-downloaded', (info) => {
  if (mainWindow.isMinimized() || !mainWindow.isFocused()) {
    mainWindow.flashFrame(true)
  }
  mainWindow.webContents.send('MainWindowUpdateDownloaded');
})