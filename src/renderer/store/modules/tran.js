const state = {
    // 资产
    assets: {},
    // 持仓       
    positionList: [],
    //今日成交
    dealList: [],
    //今日委托
    entrustList: [],
    // 需要订阅的行情
    quote: {
        sh: [],
        sz: []
    },
    //逆回购数据
    reverseList: [],
    optionalQuote: [],
    isLogin: false,
    //卖出中需要的ordinarySell
    ordinarySell: [],
    getReportFlag: false,
    accountList: [],
    isRecordOrder:false,
}

const getters = {}
const mutations = {
    ASSETS: (state, assets) => {
        state.assets = assets;
    },
    POSITION_LIST: (state, positionList) => {
        state.positionList = positionList;
    },
    ORDINARYSELL: (state, data) => {
        state.ordinarySell = data;
    },
    REVERSELIST: (state, reverseList) => {
        state.reverseList = reverseList;
    },
    // 待添加
    QUOTE: (state, list) => {
        let obj = {
            sh: {
                tickers: Array.from(
                    list.filter(
                        it => it.xtpMarketType == "XTP_MKT_SH_A" || it.exchageId == 1
                    ),
                    it => it.ticker
                ),
                exchageId: 1,
            },
            sz: {
                tickers: Array.from(
                    list.filter(
                        it => it.xtpMarketType == "XTP_MKT_SZ_A" || it.exchageId == 2
                    ),
                    it => it.ticker
                ),
                exchageId: 2,
            }
        };

        //   state.quote=


    },
    OPTIONAL_QUOTE: (state, list) => {
        state.optionalQuote = list;
    },
    OPTIONAL_QUOTE_PUSH: (state, data) => {
        state.optionalQuote.push(data);
    },
    OPTIONAL_QUOTE_SPLICE: (state, data) => {
        let i = state.optionalQuote.findIndex(it => it.ticker == data.ticker);
        i != -1 && state.optionalQuote.splice(i, 1);
    },
    LOGIN: (state) => {
        state.isLogin = true;
    },
    LOGOUT() {
        state.isLogin = false;
        state.assets = {};
        state.positionList = [];
        state.dealList = [];
        state.entrustList = [];
    },
    GETREPORTFLAG: (state, data) => {
        state.getReportFlag = data;
    },
    ACCOUNTLIST: (state, data) => {
        if (Array.isArray(data)) { 
            state.accountList = [];
        }
        else { 
            let i = state.accountList.findIndex(it => it.username == data.username);
            if (i == -1) state.accountList.push(data)
            else state.accountList.splice(i, 1, data);
        }
    },
    ISRECORDORDER: (state, data)=>{
        state.isRecordOrder = data;
    }
}

const actions = {

}

export default {
    getters,
    state,
    mutations,
    actions
}