import Vue from 'vue'
import Vuex from 'vuex'

import modules from './modules'

Vue.use(Vuex)

export default new Vuex.Store({
  modules,
  strict: process.env.NODE_ENV !== 'production',
  state: {
    connect: false,
    connect2: false,
    message: null,
    pageData: null,
    // 持仓       
    // positionList: [],

    // positionList: []
    // 平台支持的股票代码信息
    // 持仓信息
    // 资产信息
    // 今日委托
    // 今日成交
  },
  mutations: {
    SOCKET_CONNECT: (state, status) => {
      app.log("ws11111连接成功");
      state.connect = true;
    },
    SOCKET_USER_MESSAGE: (state, message) => {
      state.message = message;
    },
    // POSITION_LIST: (state, positionList) => {
    //   state.positionList = positionList;
    // },
   
    // POSITION_LIST: (state, positionList) => {
    //   console.log('22:::', positionList);
    //   state.positionList = positionList;
    // }
  },
  actions: {
    otherAction: (context, type) => {
      console.log('44');
      return true;
    },
    socket_userMessage: (context, message) => {
      console.log('33');
      context.dispatch('newMessage', message);
      context.commit('NEW_MESSAGE_RECEIVED', message);
      if (message.is_important) {
        context.dispatch('alertImportantMessage', message);
      }
    },
    // position_list: (context, positionList) => { 
    //   context.commit('POSITION_LIST', positionList); 
    // }
  }
})