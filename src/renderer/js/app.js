// const electron = require('electron')
// const os = require('os')

// const dataType = require('@/js/dataType.js')
import axios from 'axios';
import cookie from 'js-cookie';
import qs from 'qs';
import iView from 'iview';
// const package=require('../../../package.json')
import {
    dataTypeOriginal,
    dataType
} from '@/js/dataType.js'
var app = (function (document, undefined) {
    var $ = function () {};
    $.appType = 1; //1-开发；2-测试；3-生产  
    $.debug = true;
    $.env = process.env || {};
    $.package = {};
    $.config = {};
    $.isModal = localStorage.getItem("isModal_") === "false" ? false : true;
    $.isDbWithdrawal = localStorage.getItem("isDbWithdrawal_") == "true" ? true : false;
    $.orderPath = null;
    $.log = function () {
        localStorage.debug == 'true' && console.log(Object.values(arguments));
    };
    $.post = function (par) {
        console.log(JSON.stringify(par))
        // electron.shell.showItemInFolder(os.homedir())
    }
    $.getSN = function () {
        return $.Cache.getInt($.Cache.key.sn.toKeyName(), function () {
            return Math.random().toString(10).substr(2, 3);
        })
    };
    $.getServiceIP = function () {
        return $.Cache.getString($.Cache.key.serviceIP.toKeyName(), function () {
            return $.config.defaultService;
        })
    };
    // var child_process = require('child_process');
    // $.copyFile = function (f1, f2, cbk) {
    //     var cmd;
    //     if (process.platform == 'win32') {
    //         cmd = `copy /Y "${f1}" "${f2}"`;
    //     } else {
    //         cmd = `cp -rf "${f1}" "${f2}"`;
    //     }

    //     child_process.exec(cmd, cbk);
    // };
    $.ticker = {
        //普通股票
        XTP_TICKER_TYPE_STOCK: [],
        //指数
        XTP_TICKER_TYPE_INDEX: [],
        //基金
        XTP_TICKER_TYPE_FUND: [],
        //债券
        XTP_TICKER_TYPE_BOND: [],
        //未知
        XTP_TICKER_TYPE_UNKNOWN: [],
        ordinarySell: [], //普通卖出股票代码提示
        all: function () {
            let arr = [];
            // debugger
            for (let v of Object.values(app.ticker)) {
                // debugger
                if (typeof v == 'object') {
                    arr = arr.concat(v);
                }
            }
            return arr;
        }
    };
    $.tickerAll = function () {
        return [...$.ticker.XTP_TICKER_TYPE_STOCK, ...$.ticker.XTP_TICKER_TYPE_INDEX, ...$.ticker.XTP_TICKER_TYPE_FUND, ...$.ticker.XTP_TICKER_TYPE_BOND, ...$.ticker.XTP_TICKER_TYPE_UNKNOWN]
    };
    $.tickerAllList = [];
    $.dataType = dataType;
    $.dataTypeOriginal = dataTypeOriginal;
    $.tickerSearch = function (val, type) {
        let arr = [];
        let len = 8;
        let tickerList = [];
        if (typeof type === 'string') tickerList = $.ticker[type];
        else if (Array.isArray(type)) {
            type.forEach(it => {
                tickerList = tickerList.concat($.ticker[it]);
            })
        } else if (type === undefined) tickerList = $.ticker.all();
        if (val) {
            for (let i = 0; i < len + 1 - val.length; i++) {
                //debugger
                let list = tickerList.filter(it =>
                    (it.name.indexOf(val) == i ||
                        it.code.indexOf(val) == i ||
                        it.kw.indexOf(val) == i) &&
                    !arr.find(it2 => (it2.code == it.code && it2.marketId == it.marketId))
                );
                list.forEach(it => {
                    if (!arr.find(it2 => it2.code == it.code && it2.exchangeId == it.exchangeId)) {
                        arr.push(it);
                        if (arr.length == len) {
                            return arr;
                        }
                    }

                });
                if (arr.length >= len) {
                    return arr.slice(0, len);
                }
            }
        } else {
            // arr = $.ticker[type].slice(0, 6)
        }
        console.log('val::', arr);
        return arr;
    };
    $.dataTypeToName = function (v, k) {
        // debugger
        return $.dataType[k][v] ? $.dataType[k][v].name : '';
    }
    /**
     * 兼容 AMD 模块
     **/
    // if (typeof define === 'function' && define.amd) {
    //     define('app', [], function () {
    //         return $;
    //     });
    // }
    return $;
})(document);
(function ($, win) {
    // 10.25.26.184:8088 10.25.24.105:28082
    $.method = {
        'auth/login': {
            url: `proxycenter/v1/auth/login`, //'http://10.25.26.184:8888/proxyventer/v1/auth/login',// 'http://10.25.24.105:28082/proxyventer/v1/auth/login',http://10.25.26.184:8888
            type: 'post'
        },
        'user/logout': {
            url: `proxycenter/v1/user/logout`, //'http://10.25.26.184:8888/proxyventer/v1/auth/login',// 'http://10.25.24.105:28082/proxyventer/v1/auth/login',http://10.25.26.184:8888
            type: 'post'
        },
        'captcha/defaultKaptcha': {
            url: `proxycenter/v1/captcha/defaultKaptcha`,
            type: 'post'
        },
        'user/orders': {
            url: `proxycenter/v1/user/orders`,
            type: 'get'
        },
        'user/trades': {
            url: `proxycenter/v1/user/trades`,
            type: 'get'
        },
        'user/exportorders': {
            url: `proxycenter/v1/user/exportorders`,
            type: 'get'
        },
        'user/exporttrades': {
            url: `proxycenter/v1/user/exporttrades`,
            type: 'get'
        }
    };
    $.methodAlgorithm = {
        'report': {
            url: `get_task_report`,
            type: 'get'
        },
        'pause': {
            url: `pause_task`,
            type: 'post'
        },
        'resume': {
            url: `resume_task`,
            type: 'post'
        },
        'stop': {
            url: 'stop_task',
            type: 'post'
        },
        'minute': {
            url: 'get_minute_report',
            type: 'get'
        },
        'start': {
            url: 'start_task',
            type: 'post'
        }
    };
    $.ajaxOption = {
        prefix: '',
        serviceIP: '',
        port: ''
    };
    $.ajax = function (method, par, successCall, errorCall) {
        console.log('par::', par);
        let m = $.method[method];
        let opt = {
            timeout: 25000,
            method: m.type,
            url: `${$.ajaxOption.prefix}://${$.ajaxOption.serviceIP}:${$.ajaxOption.port}/${m.url}`,
        };
        m.type == 'get' ? opt.params = par : opt.data = qs.stringify(par);
        axios(opt, {
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        }).then(res => {
            console.log('res::', res);
            if (res.data.retCode == 0 || res.data.retCode == 1 || res.data.retCode == undefined) {
                successCall(res.data)
            } else {
                !!errorCall && errorCall(res.data)
            }
        }).catch(err => {
            err.msg = err.message;
            !!errorCall && errorCall(err)
        });
    };
    $.request = function (method, par, successCall, errorCall) {
        let m = $.methodAlgorithm[method];
        let opt = {
            timeout: 10000,
            method: m.type,
            url: `${JSON.parse(sessionStorage.getItem('algorithm')).baseurl}${m.url}`,
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json'
            }
        };
        m.type == 'get' ? opt.params = par : opt.data = par;
        axios(opt).then(res => {
            if (res) {
                successCall(res.data)
            } else {
                !!errorCall && errorCall(res.data)
            }
        }).catch(err => {
            err.msg = err.message;
            !!errorCall && errorCall(err)
        });
    };
    $.export = function (method, opt) {
        let a = document.createElement('a');

        let m = $.method[method];
        let url = `${$.ajaxOption.prefix}://${$.ajaxOption.serviceIP}:${$.ajaxOption.port}/${m.url}`
        let parStr = '?';
        for (let k in opt.par) {
            parStr += `${k}=${opt.par[k]}&`
        }
        url = url + parStr;
        let blob = new Blob([url], {});
        var csvUrl = URL.createObjectURL(blob);
        a.href = url
        a.target = '_blank';
        a.download = opt.fileName;
        a.click();

    }
})(app, window);
// y - tabs
(function ($, doc, win) {
    window.addEventListener(('orientationchange' in window ? 'orientationchange' : 'resize'), (function () {

        function c() {
            var d = document.documentElement;
            var cw = d.clientWidth || 750;
            d.style.fontSize = (20 * (cw / 375)) > 40 ? 40 + 'px' : (20 * (cw / 375)) + 'px';
            console.log(d.style.fontSize)
        }

        c();
        return c;
    })(), false);
})(app, document, window);
/**
 * 缓存管理
 * @param {Object} $ app类
 * @param {Object} win
 */
(function ($, win) {
    // debugger
    win.iDB = win.indexedDB || win.mozIndexedDB || win.webkitIndexedDB || win.msIndexedDB;
    win.IDBTransaction = win.IDBTransaction || win.webkitIDBTransaction || win.msIDBTransaction;
    win.IDBKeyRange = win.IDBKeyRange || win.webkitIDBKeyRange || win.msIDBKeyRange;
    $.Cache = {
        keyTime: 'keyTime',
        key: {
            user: 'user',
            sn: 'sn',
            token: 'token',
            userId: 'userId',
            model: 'model',
            modelAll: 'modelAll',
            positionTiketList: 'positionTiketList', //持仓代码
            tickerList: 'tickerList', //支持股票代码
            serviceIP: 'serviceIP',
            optionalStock: 'optionalStock',
            isShowDev: 'isShowDev',
            CSVPath: 'CSVPath',
            CSVPathAlgorithm1: 'CSVPathAlgorithm1',
            CSVPathAlgorithm2: 'CSVPathAlgorithm2',
            CSVCurrentRowNo: 'CSVCurrentRowNo',
            CSVCurrentRowNoAlgorithm1: 'CSVCurrentRowNoAlgorithm1',
            CSVCurrentRowNoAlgorithm2: 'CSVCurrentRowNoAlgorithm2',
            CSVStartByte: 'CSVStartByte',
            CSVStartByteAlgorithm1: 'CSVStartByteAlgorithm1',
            CSVStartByteAlgorithm2: 'CSVStartByteAlgorithm2',
            algorithm: 'algorithm',
            account: 'account',
            isModal: 'isModal',
            isDbWithdrawal: 'isDbWithdrawal'
        },
        //初始化缓存
        init: function () {
            $.Cache.del($.Cache.key.caskNewsId.toKeyName());
        },
        /**
         * 获取缓存中的数据
         * @param {String} k
         * @param {Function} fun 当缓存中无此key对应缓存值时，获取此值的函数
         * @param {Number} t 有效秒数
         */
        get: function (k, fun, t) {
            var v;
            //超时删除缓存中存在的原有值 
            var t1 = localStorage.getItem($.Cache.keyTime.toKeyName(k));
            t1 = t1 == null ? 0 : parseInt(t1);
            var time = new Date().getTime();
            //缓存值过期处理
            if (t1 != 0 && t1 < time) {
                localStorage.removeItem(k);
            }
            if (t != undefined) {
                localStorage.setItem($.Cache.keyTime.toKeyName(k), time + t * 1000);
            }
            v = localStorage.getItem(k);
            if (v == null) {
                if (fun != undefined) {
                    v = fun();
                    localStorage.setItem(k, v);
                }
            }
            return v;
        },
        getInt: function (k, fun, t) {
            var v = $.Cache.get(k, fun, t);
            return v == null ? 0 : parseInt(v);
        },
        set: function (k, v, t) {
            localStorage.setItem(k, v);
            if (t != undefined) {
                var time = new Date().getTime();
                localStorage.setItem($.Cache.keyTime.toKeyName(k), time + t * 1000);
            }
        },

        setObject: function (k, v, t) {
            localStorage.setItem(k, JSON.stringify(v));
            if (t != undefined) {
                var time = new Date().getTime();
                localStorage.setItem($.Cache.keyTime.toKeyName(k), time + t * 1000);
            }
        },
        getFloat: function (k, fun, t) {
            var v = $.Cache.get(k, fun, t);
            return v == null ? 0 : parseFloat(v);
        },
        getBoolean: function (k, fun, t) {
            var v = $.Cache.get(k, fun, t);
            return v == 'true' ? true : false;
        },
        getObject: function (k, fun, t) {
            var v = $.Cache.get(k, fun, t);
            return v == null ? null : JSON.parse(v);
        },
        getArray: function (k, fun, t) {
            var v = $.Cache.get(k, fun, t);
            return v == null ? [] : JSON.parse(v);
        },
        getString: function (k, fun, t) {
            var v = $.Cache.get(k, fun, t);
            return v == null ? '' : v.toString();
        },
        del: function (k) {
            localStorage.removeItem(k);
            localStorage.removeItem($.Cache.keyTime.toKeyName(k));
        },
        iHasKey: function (k) {
            k = $.Cache.keyTime.toKeyName(k);
            return localStorage.hasOwnProperty(k);
        },
        sset: function (k, v) {
            sessionStorage.setItem(k, v);
        },
        sget: function (k) {
            return sessionStorage.getItem(k);
        },
        sgetInt: function (k) {
            var v = $.Cache.sget(k);
            return isNaN(v) ? 0 : parseInt(v);
        },
        ssetObject: function (k, v) {
            sessionStorage.setItem(k, JSON.stringify(v))
        },
        sgetObject: function (k) {
            var v = $.Cache.sget(k);
            return v == null ? null : JSON.parse(v);
        },
        sdel: function (k) {
            sessionStorage.removeItem(k);
            sessionStorage.removeItem($.Cache.keyTime.toKeyName(k));
        },
        //indexeddb 存储

        version: 1, // important: only use whole numbers!

        objectStoreName: 'db',

        instance: {},

        upgrade: function (e) {

            var
                _db = e.target.result,
                names = _db.objectStoreNames,
                name = $.Cache.objectStoreName;

            if (!names.contains(name)) {

                _db.createObjectStore(
                    name, {
                        keyPath: 'key',
                        autoIncrement: true
                    });
            }
        },

        errorHandler: function (error) {
            //			window.alert('error: ' + error.target.code);

        },

        open: function (callback) {

            var request = win.iDB.open(
                $.Cache.objectStoreName, $.Cache.version);

            request.onerror = $.Cache.errorHandler;

            request.onupgradeneeded = $.Cache.upgrade;

            request.onsuccess = function (e) {

                $.Cache.instance = request.result;

                $.Cache.instance.onerror =
                    $.Cache.errorHandler;

                callback();
            };
        },

        getObjectStore: function (mode) {

            var txn, store;

            mode = mode || 'readonly';

            txn = $.Cache.instance.transaction(
                [$.Cache.objectStoreName], mode);

            store = txn.objectStore(
                $.Cache.objectStoreName);

            return store;
        },

        /**
         * 设置缓存
         * @param {String} key 键
         * @param {Object} data 值
         * @param {Number} t    有效期 秒
         * @param {Function} callback 成功后回调函数
         */
        iset: function (k, v, t, callback) {
            t = t || 864000; //默认有效期为10天

            var time = new Date().getTime();
            k = $.Cache.keyTime.toKeyName(k);
            $.Cache.set(k, time + t * 1000);

            try {
                $.Cache.open(function () {
                    var store, request,
                        mode = 'readwrite',
                        obj = {
                            key: k,
                            val: v,
                        }
                    store = $.Cache.getObjectStore(mode),
                        request = k ?
                        store.put(obj) :
                        store.add(obj);
                    request.onsuccess = callback;
                });
            } catch (e) {

            }

        },
        iget: function (k, callback) {
            k = $.Cache.keyTime.toKeyName(k);
            try {
                if (mui.os.plus) {

                    callback(plus.storage.getItem(k));
                } else {
                    $.Cache.open(function () {
                        var mode = 'readwrite',
                            store = $.Cache.getObjectStore(mode);
                        var request = store.get(k);
                        request.onsuccess = function (e) {
                            //							var result = e.target.result && new Date().getTime() > e.target.result.useTime ? null : e.target.result;
                            //							result && $.Cache.iset(result.key, result.val, result.useT);
                            callback(result ? result.val : null);
                        };
                    });
                }
            } catch (e) {
                callback(null);
            }

        },
        igetObject: function (k, callback) {
            k = $.Cache.keyTime.toKeyName(k);
            try {
                if (mui.os.plus) {
                    var v = plus.storage.getItem(k);
                    callback(v && JSON.parse(v));
                } else {
                    $.Cache.open(function () {
                        var mode = 'readwrite',
                            store = $.Cache.getObjectStore(mode);
                        var request = store.get(k);
                        request.onsuccess = function (e) {
                            //							result && $.Cache.iset(result.key, result.val, result.useT);
                            callback(e.target.result ? e.target.result.val : null);
                        };
                    });
                }
            } catch (e) {
                callback(null);
            }

        },
        igetAll: function (callback) {

            $.Cache.open(function () {

                var
                    store = $.Cache.getObjectStore(),
                    cursor = store.openCursor(),
                    data = [];

                cursor.onsuccess = function (e) {

                    var result = e.target.result;

                    if (result &&
                        result !== null) {

                        data.push(result.value);
                        result.continue();

                    } else {

                        callback(data);
                    }
                };

            });
        },
        idelete: function (k, callback) {
            $.Cache.open(function () {
                var
                    mode = 'readwrite',
                    store, request;

                store = $.Cache.getObjectStore(mode);

                request = store.delete(k);

                request.onsuccess = callback;
            });
        },

        ideleteAll: function (callback) {
            var ks = [];
            for (var i = 0; i >= 0; i++) {
                var k = localStorage.key(i);
                if (k) {
                    if (k.indexOf($.Cache.keyTime.toKeyName()) == 0) {
                        ks.push(k)
                    }
                } else {
                    i = -2;
                }
            }
            for (var i = 0; i < ks.length; i++) {
                var k = ks[i];
                $.Cache.del(k);
            }
            $.Cache.open(function () {

                var mode, store, request;

                mode = 'readwrite';
                store = $.Cache.getObjectStore(mode);
                request = store.clear();

                !!callback && (request.onsuccess = callback);
            });


        },
        /**
         * 清除过期的缓存
         */
        iclearExpired: function () {
            var ks = [];
            for (var i = 0; i >= 0; i++) {
                var k = localStorage.key(i);
                if (k) {
                    if (k.indexOf($.Cache.keyTime.toKeyName()) == 0) {
                        ks.push(k)
                    }
                } else {
                    i = -2;
                }
            }
            for (var i = 0; i < ks.length; i++) {
                var k = ks[i];
                var t1 = $.Cache.getInt(k);
                var time = new Date().getTime();
                //缓存值过期处理
                if (t1 != 0 && t1 < time) {
                    $.Cache.del(k);
                    $.Cache.idelete(k)
                }

            }

        },

    };
    /**
     * 登录用户类，所有需要用到的用户信息都从此类获取
     */
    $.User = {

        username: '',
        sessionIdMd5: '',

        set: function (u) {
            // $.User.setToken(u.token);
            // $.User.setUserId(u.uid);

            $.Cache.ssetObject($.Cache.key.user.toKeyName(), u);
        },
        get: function () {
            var u = $.Cache.sgetObject($.Cache.key.user.toKeyName());
            return u || $.User;
        },
        setToken: function (token) {
            $.Cache.set($.Cache.key.token.toKeyName(), token);
        },
        getToken: function () {
            return $.Cache.getString($.Cache.key.token.toKeyName());
        },

        setUserId: function (v) {
            $.Cache.set($.Cache.key.userId.toKeyName(), v);
        },
        getUserId: function () {
            return $.Cache.getString($.Cache.key.userId.toKeyName())
        },
        clear: function () {
            $.Cache.sdel($.Cache.key.user.toKeyName());
            $.Cache.del($.Cache.key.sn.toKeyName());
            $.Cache.del($.Cache.key.token.toKeyName());
        }

    };
})(app, window);
window.app = app;
export {
    app
}