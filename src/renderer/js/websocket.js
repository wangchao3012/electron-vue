const sendData = {
    monitor: "get_task_report",
    task: "subscribe_task_report",
    minute: "subscribe_minute_report",
    getMinute:"get_minute_report"
},
export function websocket(data = {}) {
    let self = this;
    self.username = JSON.parse(sessionStorage.getItem("user_")) == null ? null : JSON.parse(sessionStorage.getItem("user_")).username;
    if (self.session_id) {
        websock = new WebSocket(`ws://10.25.24.183:1234/report?session_id=${self.session_id}`);
    }
    websock.onopen = self.websocketOpen;
    websock.onmessage = self.websocketMessage;
    websock.onclose = self.websockClose;
    websock.onerror = self.websockError;
    self.websock = websock;
    self.$store.commit('WEBSOCKET',websock);
    self.$store.commit("ISWEBSOCKET");
}