let numeral = require('numeral');

if (String.prototype.toKeyName === undefined) { // fix for iOS 3.2
    /**
     * 拼接缓存key名称
     */
    let chnNumChar = ["零", "一", "二", "三", "四", "五", "六", "七", "八", "九"];
    let chnUnitSection = ["", "万", "亿", "万亿", "亿亿"];
    let chnUnitChar = ["", "十", "百", "千"];
    String.prototype.toKeyName = function () {
        let s = this + '_';
        for (let i = 0; i < arguments.length; i++) {
            s += arguments[i] + '_';
        }
        return s;
    };
    // 对Date的扩展，将 Date 转化为指定格式的String
    // 月(M)、日(d)、小时(h)、分(m)、秒(s)、季度(q) 可以用 1-2 个占位符，
    // 年(y)可以用 1-4 个占位符，毫秒(S)只能用 1 个占位符(是 1-3 位的数字)
    // 例子：
    // (new Date()).toDate("yyyy-MM-dd hh:mm:ss") ==> 2006-07-02 08:09:04.423
    // (new Date()).Format("yyyy-M-d h:m:s.S")      ==> 2006-7-2 8:9:4.18
    //时间戳转日期
    Date.prototype.toLocaleString = function () {
        return this.getFullYear() + "-" + (this.getMonth() + 1) + "-" + this.getDate() + "- " + this.getHours() + ":" + this.getMinutes() + ":" + this.getSeconds();
    };
    Date.prototype.Format = function (fmt) { // author: meizz
        let d = this;
        let o = {
            'M+': d.getMonth() + 1, // 月份
            'd+': d.getDate(), // 日
            'h+': d.getHours(), // 小时
            'm+': d.getMinutes(), // 分
            's+': d.getSeconds(), // 秒
            'q+': Math.floor((d.getMonth() + 3) / 3), // 季度
            'S': d.getMilliseconds() // 毫秒
        };
        if (/(y+)/.test(fmt)) {
            fmt = fmt.replace(RegExp.$1, (d.getFullYear() + '').substr(4 - RegExp.$1.length));
        }
        for (let k in o) {
            if (new RegExp('(' + k + ')').test(fmt)) {
                fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (('00' + o[k]).substr(('' + o[k]).length)));
            }
        }
        return fmt.replace('T', ' ').replace('+08:00', '');
    };

    String.prototype.Format = function (fmt) {
        return new Date(this.replace(/-/g, '/')).Format(fmt);
    };
    Number.prototype.toDate = function (fmt) { // author: meizz
        if (this == 0) return '';
        let n = this.toString();
        let d = new Date(n.substr(0, 4) + '-' + n.substr(4, 2) + '-' + n.substr(6, 2) + ' ' + n.substr(8, 2) + ':' + n.substr(10, 2) + ':' + n.substr(12, 2) + '.' + n.substr(14, 3));
        return d.Format(fmt);
    };
    Number.prototype.Format = function (fmt) { // author: meizz
        // return numeral(this).format('0,0.00'); 
        return numeral(this).format(fmt);
    };
    String.prototype.toDate = function (fmt) { // author: meizz
        if (this == 0) return '';
        return this.substr(0, 4) + '-' + this.substr(4, 2) + '-' + this.substr(6, 2) + ' ' + this.substr(8, 2) + ':' + this.substr(10, 2) + ':' + this.substr(12, 2)
    };
    Number.prototype.SectionToChinese = (section) => {
        let strIns = '', chnStr = '';
        let unitPos = 0;
        let zero = true;
        while (section > 0) {
            let v = section % 10;
            if (v === 0) {
                if (!zero) {
                    zero = true;
                    chnStr = chnNumChar[v] + chnStr;
                }
            } else {
                zero = false;
                strIns = chnNumChar[v];
                strIns += chnUnitChar[unitPos];
                chnStr = strIns + chnStr;
            }
            unitPos++;
            section = Math.floor(section / 10);
        }
        return chnStr;
    }
    Number.prototype.NumberToChinese = (num) => {
        let unitPos = 0;
        let strIns = '', chnStr = '';
        let needZero = false;

        if (num === 0) {
            return chnNumChar[0];
        }

        while (num > 0) {
            let section = num % 10000;
            if (needZero) {
                chnStr = chnNumChar[0] + chnStr;
            }
            strIns = section.SectionToChinese(section);
            strIns += (section !== 0) ? chnUnitSection[unitPos] : chnUnitSection[0];
            chnStr = strIns + chnStr;
            needZero = (section < 1000) && (section > 0);
            num = Math.floor(num / 10000);
            unitPos++;
        }

        return chnStr;
    }
}
Object.setPrototypeOf = Object.setPrototypeOf || function (obj, proto) {
    obj['__proto__'] = proto;
    return obj;
};