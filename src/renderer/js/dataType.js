const dataTypeOriginal = {
    //1现金替代标识定义
    ETF_REPLACE_TYPE: {
        ERT_CASH_FORBIDDEN: {
            name: '禁止现金替代',
            i: 0
        },
        ERT_CASH_OPTIONAL: {
            name: '可以现金替代',
            i: 1
        },
        ERT_CASH_MUST: {
            name: '必须现金替代',
            i: 2
        },
        EPT_INVALID : {
            name: '无效值',
            i: 3
        }
    },
    //1.1.16新增
    //2账户类型
    XTP_ACCOUNT_TYPE: {
        XTP_ACCOUNT_NORMAL: {
            name:'普通账户',
            i: 0
        },
        XTP_ACCOUNT_CREDIT: {
            name: '信用账户',
            i: 1
        },
        XTP_ACCOUNT_DERIVE: {
            name: '衍生品账户',
            i: 2
        },
        XTP_ACCOUNT_UNKNOWN: {
            name: '未知',
            i: 3
        }
    },
    //3证券业务类型
    XTP_BUSINESS_TYPE: {
        XTP_BUSINESS_TYPE_CASH: {
            name: '普通股票',
            i: 0
        },
        XTP_BUSINESS_TYPE_IPOS: {
            name: '新股申购',
            i: 1
        },
        XTP_BUSINESS_TYPE_REPO: {
            name: '回购业务',
            i: 2
        },
        XTP_BUSINESS_TYPE_ETF: {
            name: 'ETF申赎',
            i: 3
        },
        XTP_BUSINESS_TYPE_MARGIN: {
            name: '融资融券',
            i: 4
        },
        XTP_BUSINESS_TYPE_DESIGNATION : {
            name: '转托管',
            i: 5
        },
        XTP_BUSINESS_TYPE_ALLOTMENT: {
            name: '配股',
            i: 6
        },
        XTP_BUSINESS_TYPE_STRUCTURED_FUND_PURCHASE_REDEMPTION: {
            name: '分级基金申赎',
            i: 7
        },
        XTP_BUSINESS_TYPE_STRUCTURED_FUND_SPLIT_MERGE: {
            name: '分级基金拆分合并',
            i: 8
        },
        XTP_BUSINESS_TYPE_MONEY_FUND  : {
            name: '货币基金',
            i: 9
        },
        XTP_BUSINESS_TYPE_UNKNOWN: {
            name: '未知类型',
            i: 10
        },
        
    },
    //4交易所类型
    XTP_EXCHANGE_TYPE: {
        XTP_EXCHANGE_SH: {
            name: '沪A',
            i: 1,
            marketId:2
        },
        XTP_EXCHANGE_SZ: {
            name: '深A',
            i: 2,
            marketId:1
        },
        XTP_EXCHANGE_UNKNOWN: {
            name: '',
            i: 3
        }, 
    },
    //5柜台资金操作结果
    XTP_FUND_OPER_STATUS: {
        XTP_FUND_OPER_PROCESSING: {
            name: '处理中',
            i: 0
        },
        XTP_FUND_OPER_SUCCESS: {
            name: '成功',
            i: 1
        },
        XTP_FUND_OPER_FAILED: {
            name: '失败',
            i: 2
        },
        XTP_FUND_OPER_SUBMITTED: {
            name: '已提交',
            i: 3
        },
        XTP_FUND_OPER_UNKNOWN: {
            name: '未知',
            i: 4
        }
    },
    //6资金流转方向类型
    XTP_FUND_TRANSFER_TYPE: {
        XTP_FUND_TRANSFER_OUT: {
            name: '转出',
            i: 0
        },
        XTP_FUND_TRANSFER_IN: {
            name: '转入',
            i: 1
        },
        XTP_FUND_TRANSFER_UNKNOWN: {
            name: '未知',
            i: 2
        }
    },
    //7,日志输出级别类型
    XTP_LOG_LEVEL: {
        XTP_LOG_LEVEL_FATAL: {
            name: '严重错误级别',
            i: 0
        },
        XTP_LOG_LEVEL_ERROR: {
            name: '错误级别',
            i: 1
        },
        XTP_LOG_LEVEL_WARNING: {
            name: '警告级别',
            i: 2
        },
        XTP_LOG_LEVEL_INFO: {
            name: 'info级别',
            i: 3
        },
        XTP_LOG_LEVEL_DEBUG: {
            name: 'debug级别',
            i: 4
        },
        XTP_LOG_LEVEL_TRACE: {
            name: 'trace级别',
            i: 5
        }
    },
    //8，市场类型
    XTP_MARKET_TYPE: {
        XTP_MKT_INIT: {
            name: '',
            i: 0
        },
        XTP_MKT_SZ_A: {
            name: '深A',
            i: 1,
            exchangeId:2
        },
        XTP_MKT_SH_A: {
            name: '沪A',
            i: 2, 
            exchangeId:1
        },
        XTP_MKT_UNKNOWN: {
            name: '未知',
            i: 3,
        },
    },
    //9报单操作状态
    XTP_ORDER_ACTION_STATUS_TYPE: {
        XTP_ORDER_ACTION_STATUS_SUBMITTED: {
            name: "已经提交",
            i: 1
        },
        XTP_ORDER_ACTION_STATUS_ACCEPTED: {
            name: "已经接受",
            i: 2
        },
        XTP_ORDER_ACTION_STATUS_REJECTED: {
            name: "已经被拒绝",
            i: 3
        }
    },
    //10报单状态类型
    XTP_ORDER_STATUS_TYPE: {
        XTP_ORDER_STATUS_INIT: {
            name: '初始化',
            i: 0
        },
        XTP_ORDER_STATUS_ALLTRADED: {
            name: '全部成交',
            i: 1
        },
        XTP_ORDER_STATUS_PARTTRADEDQUEUEING: {
            name: '部分成交',
            i: 2
        },
        XTP_ORDER_STATUS_PARTTRADEDNOTQUEUEING: {
            name: '部分撤单',
            i: 3
        },
        XTP_ORDER_STATUS_NOTRADEQUEUEING: {
            name: '未成交',
            i: 4
        },
        XTP_ORDER_STATUS_CANCELED: {
            name: '已撤单',
            i: 5
        },
        XTP_ORDER_STATUS_REJECTED: {
            name: '已拒绝',
            i: 6
        },
        XTP_ORDER_STATUS_UNKNOWN: {
            name: '未知',
            i: 7
        }
    },
    //11报单提交状态类型
    XTP_ORDER_SUBMIT_STATUS_TYPE: {
        XTP_ORDER_SUBMIT_STATUS_INSERT_SUBMITTED: {
            name: '已提交',
            i: 1
        },
        XTP_ORDER_SUBMIT_STATUS_INSERT_ACCEPTED: {
            name: '已接受',
            i: 2
        },
        XTP_ORDER_SUBMIT_STATUS_INSERT_REJECTED: {
            name: '已拒绝',
            i: 3
        },
        XTP_ORDER_SUBMIT_STATUS_CANCEL_SUBMITTED: {
            name: '已提交',
            i: 4
        },
        XTP_ORDER_SUBMIT_STATUS_CANCEL_REJECTED: {
            name: '已拒绝',
            i: 5
        },
        XTP_ORDER_SUBMIT_STATUS_CANCEL_ACCEPTED: {
            name: '已接受',
            i: 6
        }
    },
    ORDER_SUBMIT_STATUS_TYPE: {
        ORDER_SUBMIT_STATUS_INSERT_SUBMITTED: {
            name: '已提交',
            i: 1
        },
        ORDER_SUBMIT_STATUS_INSERT_ACCEPTED: {
            name: '已接受',
            i: 2
        },
        ORDER_SUBMIT_STATUS_INSERT_REJECTED: {
            name: '已拒绝',
            i: 3
        },
        ORDER_SUBMIT_STATUS_CANCEL_SUBMITTED: {
            name: '撤单已提交',
            i: 4
        },
        ORDER_SUBMIT_STATUS_CANCEL_REJECTED: {
            name: '撤单已拒绝',
            i: 5
        },
        ORDER_SUBMIT_STATUS_CANCEL_ACCEPTED: {
            name: '撤单已接受',
            i: 6
        }
    },
    //12价格类型
    XTP_PRICE_TYPE: {
        XTP_PRICE_LIMIT: {
            name: '限价',
            i: 1, 
            marketId:[1,2]
        },
        XTP_PRICE_BEST_OR_CANCEL: {
            name: '即成剩撤',
            i: 2, 
            marketId:[1]
        },
        XTP_PRICE_BEST5_OR_LIMIT: {
            name: '五档即成转限价',
            i: 3, 
            marketId:[2]
        },
        XTP_PRICE_BEST5_OR_CANCEL: {
            name: '五档即成转撤销',
            i: 4, 
            marketId:[1,2]
        },
        XTP_PRICE_ALL_OR_CANCEL: {
            name: '全部成交或撤销',
            i: 5, 
            marketId:[1]
        },
        XTP_PRICE_FORWARD_BEST: {
            name: '本方最优',
            i: 6, 
            marketId:[1]
        },
        XTP_PRICE_REVERSE_BEST_LIMIT: {
            name: '对方最优限价',
            i: 7, 
            marketId:[1]
        }, 
    },
    PRICE_TYPE: {
        LIMIT: {
            name: '限价',
            i: 1, 
            marketId:[1,2]
        },
        BEST_OR_CANCEL: {
            name: '即成剩撤',
            i: 2, 
            marketId:[1]
        },
        BEST5_OR_LIMIT: {
            name: '五档即成转限价',
            i: 3, 
            marketId:[2]
        },
        BEST5_OR_CANCEL: {
            name: '五档即成转撤销',
            i: 4, 
            marketId:[1,2]
        },
        ALL_OR_CANCEL: {
            name: '全部成交或撤销',
            i: 5, 
            marketId:[1]
        },
        FORWARD_BEST: {
            name: '本方最优',
            i: 6, 
            marketId:[1]
        },
        REVERSE_BEST_LIMIT: {
            name: '对方最优限价',
            i: 7, 
            marketId:[1]
        },
        LIMIT_OR_CANCEL: {
            name: '期权限价申报FOK',
            i: 8, 
            marketId:[1]
        },
        TYPE_UNKNOWN: {
            name: '未知',
            i: 9, 
            marketId:[1]
        },
    },
    TRADE_TYPE: {
        XTP_TRDT_COMMON: {
            name: '普通成交',
            i: 0, 
        },
        XTP_TRDT_CASH: {
            name: '现金替代',
            i: 1, 
        },
        XTP_TRDT_PRIMARY: {
            name: '一级成交',
            i: 2, 
        },
    },
    //13通讯传输协议方式
    XTP_PROTOCOL_TYPE: {
        XTP_PROTOCOL_TCP : {
            name: '采用TCP方式传输',
            i: 1
        },
        XTP_PROTOCOL_UDP: {
            name: '采用UDP方式传输（目前暂未支持)',
            i: 2
        }
    },
    //14买卖方向类型
    XTP_SIDE_TYPE: {
        XTP_SIDE_BUY: {
            name: "买",
            i: 1
        },
        XTP_SIDE_SELL: {
            name: "卖",
            i: 2
        },
        XTP_SIDE_BUY_OPEN: {
            name: "买开(暂未支持)",
            i: 3
        },
        XTP_SIDE_SELL_OPEN: {
            name: "卖开(暂未支持)",
            i: 4
        },
        XTP_SIDE_BUY_CLOSE: {
            name: '买平(暂未支持)',
            i: 5
        },
        XTP_SIDE_SELL_CLOSE: {
            name: '卖平(暂未支持)',
            i: 6
        },
        XTP_SIDE_PURCHASE: {
            name: '申购',
            i: 7
        },
        XTP_SIDE_REDEMPTION: {
            name: '赎回',
            i: 8
        },
        XTP_SIDE_SPLIT: {
            name: '拆分',
            i: 9
        },
        XTP_SIDE_MERGE: {
            name: '合并',
            i: 10
        },
        XTP_SIDE_UNKNOWN: {
            name: '未知',
            i: 11
        }
    },
    SIDE_TYPE: {
        BUY: {
            name: "买",
            i: 1
        },
        SELL: {
            name: "卖",
            i: 2
        },
        UNKNOWN: {
            name: '未知',
            i: 3
        }
    },
    //15,基金当天拆分合并状态类型
    XTP_SPLIT_MERGE_STATUS: {
        XTP_SPLIT_MERGE_STATUS_ALLOW: {
            name: '允许拆分和合并',
            i: 0
        },
        XTP_SPLIT_MERGE_STATUS_ONLY_SPLIT: {
            name: '只允许拆分，不允许合并',
            i: 1
        },
        XTP_SPLIT_MERGE_STATUS_ONLY_MERGE: {
            name: '只允许合并，不允许拆分',
            i: 2
        },
        XTP_SPLIT_MERGE_STATUS_FORBIDDEN: {
            name: '不允许拆分合并',
            i: 3
        }
    },
    //16,逐笔回报类型
    XTP_TBT_TYPE: {
        XTP_TBT_ENTRUST: {
            name: '逐笔委托',
            i: 1
        },
        XTP_TBT_TRADE: {
            name: '逐笔成交',
            i: 2
        }
    },
    //17公有流（订单响应、成交回报）重传方式
    XTP_TE_RESUME_TYPE: {
        XTP_TERT_RESTART: {
            name: '从本交易日开始重传',
            i: 0
        },
        XTP_TERT_RESUME: {
            name: '从上次收到的续传',
            i: 1
        },
        XTP_TERT_QUICK: {
            name: '登陆后内容',
            i: 2
        }
    },
    //18证券类型
    XTP_TICKER_TYPE: {
        XTP_TICKER_TYPE_STOCK: {
            name: '普通股票',
            i: 0
        },
        XTP_TICKER_TYPE_INDEX: {
            name: '指数',
            i: 1
        },
        XTP_TICKER_TYPE_FUND: {
            name: '基金',
            i: 2
        },
        XTP_TICKER_TYPE_BOND: {
            name: '债券',
            i: 3
        },
        XTP_TICKER_TYPE_UNKNOWN: {
            name: '未知',
            i: 4
        }
    },
    //未知类型
    TXTPTradeTypeType: {
        XTP_TRDT_Common: {
            name: '普通成交',
            i: 0
        },
        XTP_TRDT_OptionsExecution: {
            name: '期权执行',
            i: 1
        },
        XTP_TRDT_OTC: {
            name: 'OTC成交',
            i: 2
        },
        XTP_TRDT_EFPDerived: {
            name: '期转现衍生成交',
            i: 3
        }
    },
    ALGORITHM_STATUS: {
        READY: {
            name: '准备中',
            i: 0
        },
        IN_PROGRESS: {
            name: '进行中',
            i: 1
        },
        PAUSED: {
            name: '已暂停',
            i: 2
        },
        STOPPED: {
            name: '已停止',
            i: 3
        },
        FINISHED: {
            name: '已完成',
            i: 4
        },
        CAPITAL_INSUFFICIENT_ERROR: {
            name: '资金不足',
            i: 5
        },
        SECURITY_INSUFFICIENT_ERROR: {
            name: '持仓不足',
            i: 6
        },
        UNKNOWN: {
            name: '未知',
            i:7
        }
    }
} 
var dataType=JSON.parse(JSON.stringify(dataTypeOriginal) )   //Object.assign({},dataTypeOriginal);
for (let k in dataType) {
    for (let k1 in dataType[k]) {
        dataType[k][dataType[k][k1].i] = {
            name: dataType[k][k1].name
        }
    }
}

// debugger 
export { dataTypeOriginal, dataType  }
// module.exports = dataType;