import Vue from 'vue'
import Router from 'vue-router'
// import trade_index from './components/trade/index' 
// import entrust_index from '../page/trade/general/entrust/index'

// const order_index = () => import('../page/trade/general/order/index')

Vue.use(Router)

var router = new Router({
  routes: [{
    path: '/',
    name: 'index',
    // redirect: '/trade',
    component: require('@/page/index').default,
    children: [
      {
        path: 'trade',
        // redirect: '/trade/general',
        component: require('@/page/trade/index').default,
        children: [
          {
            // 普通交易
            path: 'general',
            // redirect: '/trade/general/order',
            component: require('@/page/trade/general/index').default,
            children: [{
              // 下单
              name: 'trade_general_order',
              path: 'order',
              component: require('@/page/trade/general/order/index').default,
            },
            // {
            //   path: '/trade/general/order',
            //   component: require('@/page/trade/general/order/index').default,
            // }, 
            // {
            //   //二级页面中委托查询子页面
            //   path: '/trade/general/entrust',
            //   // component: require('@/page/trade/general/entrust/index').default,
            //   component: entrust_index
            // },
            // {
            //   //二级页面中委托查询子页面
            //   path: '/trade/general/entrustNew',
            //   // component: require('@/page/trade/general/entrust/index').default,
            //   component: require('@/page/trade/general/entrustNew/index').default
            // },
            // {
            //   // 持仓
            //   path: '/trade/general/positions',
            //   component: require('@/page/trade/general/positions/index').default,
            // },
            // {
            //   //成交查询
            //   path: '/trade/general/deal/list',
            //   component: require('@/page/trade/general/deal/list').default,
            // },
            // {
            //   //资产净值
            //   path: '/trade/general/gradingFund/list',
            //   component: require('@/page/trade/general/gradingFund/list').default,
            // },
            {
              //新添加的国债逆回购页面
              path: '/trade/general/newSharePurchase',
              name: 'newSharePurchase',
              component: require('@/page/trade/general/newSharePurchase/index').default
            },
            {
              //新股申购页面
              path: '/trade/general/newStockBuy',
              name: 'newStockBuy',
              component: require('@/page/trade/general/newStockBuy/index').default
            },
            {
              //配股页面
              path: '/trade/general/allotment',
              name: 'allotment',
              component: require('@/page/trade/general/allotment/index').default
            },
            {
              //资金划拨
              path: '/trade/general/fundsTransfer',
              name: 'fundsTransfer',
              component: require('@/page/trade/general/fundsTransfer/index').default
            }
            ]
          },
          {
            path: 'ETF',
            component: require('@/page/trade/ETF/index').default,
            children: [
              {
                //ETF申赎
                path: 'redemption',
                name: 'redemption',
                component: require('@/page/trade/ETF/redemption/index').default,
              },
              {
                //ETF清单
                path: 'etf',
                component: require('@/page/trade/ETF/etf/index').default,
              },
              {
                //ETF股票篮
                path: 'basket',
                component: require('@/page/trade/ETF/basket/index').default,
              },
            ]
          },
          {
            path: 'fund',
            component: require('@/page/trade/fund/index').default,
          },

          {
            //二级页面中其他交易
            path: 'basket',
            name: 'trade_basket',
            component: require('@/page/trade/basket/index').default,

          },
          {
            //二级页面中其他交易
            path: 'other',
            component: require('@/page/trade/other/index').default,
            children: [
              {
                //持仓查询
                path: '/trade/other/positions',
                component: require('@/page/trade/other/positions/index').default,
              }, {
                //委托查询
                path: '/trade/other/entrust',
                component: require('@/page/trade/other/entrust/index').default,
              },
              {
                //成交查询
                path: '/trade/other/deal',
                component: require('@/page/trade/other/deal/list').default
              }
            ]
          },
        ]
      },
      {
        path: 'quotes',
        component: require('@/page/quotes/index').default,
        children: [{
          path: 'optional',
          name: 'quotes_optional',
          component: require('@/page/quotes/realTime/optional').default,
        },{
          path: 'position',
          name: 'quotes_position',
          component: require('@/page/quotes/realTime/position').default,
        }]
      },
      {
        //一级菜单中策略页面中的算法拆单
        path: 'strategies',
        component: require('@/page/strategies/index').default,
        children: [{
          // 算法拆单中子页面
          path: 'order',
          name: 'strategies_order',
          component: require('@/page/strategies/order/index').default,
        },]
      },
      {
        path: '/news',
        component: require('@/page/news/index').default,
        children: [{
          //一级菜单咨询中每日必读
          path: 'read',
          name: 'news_read',
          component: require('@/page/news/read/index').default,
        },]
      },
      {
        path: '/account',
        component: require('@/page/account/index').default,
        // redirect: '/accountOperating',
        children: [{
          path: 'accountOperat',
          component: require('@/page/account/accountOperat/index').default,
        },]
      },
      {
        path: 'strategy',
        component: require('@/page/strategy/index').default,
        children: [
          {
            //算法交易页面
            path: 'algorithm',
            component: require('@/page/strategy/algorithm/index').default,
            children: [
              {
                path: 'strategies',
                component: require('@/page/strategy/algorithm/strategies/index').default,
              },
              {
                path: 'order',
                component: require('@/page/strategy/algorithm/order/index').default,
              }
            ]
          },
        ]
      },
      {
        path: '/test',
        component: require('@/page/test/index').default,
      },
    ]
  },
  {
    path: '/login',
    name: 'login',
    component: require('@/page/login').default,
  },
  {
    path: '*',
    redirect: '/'
  },
  ]
})

// router.beforeRouteEnter (() => {

//   let num = document.querySelectorAll('.i-table').length;

//   console.warn('onReady :::::::::::::::::::', num, document.querySelectorAll('.i-table'));
// })



router.beforeEach((to, from, next) => {
  //  设置默认跳转路由
  // debugger
  if (to.path == '/' || to.path == '/trade' || to.path == '/trade/general') {

    // router.push({
    //   name: 'trade_general_order'
    // })
    next('/trade/general/order')
    // next('/trade/general/entrust')
    // next()
  } else if (to.path == '/strategy') {
    next('/strategy/algorithm/strategies')
  } else if (to.path == '/quotes') {
    next('/quotes/optional')
  } else if (to.path == '/strategies') {
    next('/strategies/order')
  } else if (to.path == '/news') {
    next('/news/read')
  } else if (to.path == '/trade/ETF') {
    next('/trade/ETF/redemption')
  } else if (to.path == '/account') {
    next('/account/accountOperat')
  } else if (to.path == '/trade/other') {
    next('/trade/other/positions')
  }
  else {
    next()
  }
  let num = document.querySelectorAll('.i-table').length;

  // console.warn('beforeEach:::::::::::::::::::', num, document.querySelectorAll('.i-table'));
})
// beforeRouteUpdate afterEach
// router.afterEach((to, from, next) => {
//   // next()
//   let self = this;
//   // debugger
//   let num = document.querySelectorAll('.i-table').length;

//   console.warn('afterEach:::::::::::::::::::', num, document.querySelectorAll('.i-table'));

// })
// router.beforeResolve((to, from, next) => {

//   let self = this;
//   next(vm => {
//     // debugger
//     vm.aaa();
//     // = function () {
//     //   console.warn('beforeResolve33333');
//     // }();
//     // let num = document.querySelectorAll('.i-table').length;

//     // console.warn('beforeResolve222 :::::::::::::::::::', num, document.querySelectorAll('.i-table'));
//   })
//   // debugger
//   // debugger
//   let num = document.querySelectorAll('.i-table').length;

//   console.warn('beforeResolve :::::::::::::::::::', num, document.querySelectorAll('.i-table'));

// })


export default router;