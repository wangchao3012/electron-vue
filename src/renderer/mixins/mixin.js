import { app } from '@/js/app.js';
let pg = require('../../../package.json');

app.config = Object.assign(CONFIG, pg);

app.env = process.env || {};
console.log('app.env:::', app.env);

let fs = null,
    readline = null,
    chokidar = null;
if (!process.env.IS_WEB) {
    app.env.isPlus = true;
    fs = require("fs")
    readline = require('readline');
    chokidar = require("chokidar");
}

var ChangeTableMixin = {
    mounted: function() {
        var self = this;
        self.$nextTick(function() {
            $('table').width(0);
            var lastwidth =$(".ivu-table-tbody tr").eq(1).children("td:last-child").width();
            $("table colgroup").children("col:last-child").width(lastwidth);
       });
    }
}

/**
 * 读写CSV文件使用
 */
var FileReaderMixin = {
    data: function () {
        return {
            fs: fs,
            readline: readline,
            chokidar: chokidar
        }
    }
}
/**
 * 组件需要读取全局配置文件时使用
 */
var GlobalConfogMixin = {
    data: function () {
        return {
            config: app.config
        }
    }
}
/**
 * 组件需要读取全局环境变量时使用
 */
var GlobalEnvMixin = {
    data: function () {
        return {
            env: app.env
        }
    }
}
/**
 * 数据类型
 */
var DataTypeMixin = {
    data: function () {
        return {
            dataType: app.dataTypeOriginal,
            dataTypeAll: app.dataType
        }
    }
}

/**
 * 中文数字
 */
var NumChineseMixin = {
    data: function () {
        return {
            numChinese: {
                1: '一',
                2: '二',
                3: '三',
                4: '四',
                5: '五',
                6: '六',
                7: '七',
                8: '八',
                9: '九',
                10: '十',
            }
        }
    }
}

export { ChangeTableMixin, FileReaderMixin, GlobalConfogMixin, GlobalEnvMixin, DataTypeMixin, NumChineseMixin } ;