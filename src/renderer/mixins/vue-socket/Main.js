import Observer from './Observer'
import Emitter from './Emitter'

export default {

    install(Vue, store, connection, connection2) {

        if (!connection) throw new Error("[Vue-Socket.io] cannot locate connection")
    
        let observer = new Observer(connection, store)
        let observer2 = new Observer(connection2, store)
       
        Vue.prototype.$socket = observer.Socket;
        Vue.prototype.$socket2 = observer2.Socket;

        Vue.mixin({
            created() {
                let sockets = this.$options['sockets']
                this.$options.sockets = new Proxy({}, {
                    set: (target, key, value) => {

                        Emitter.addListener(key, value, this)
                        target[key] = value
                        return true;
                    },
                    deleteProperty: (target, key) => {
                        Emitter.removeListener(key, this.$options.sockets[key], this)
                        delete target.key;
                        return true
                    }
                })

                let sockets2 = this.$options['sockets2']
                this.$options.sockets2 = new Proxy({}, {
                    set: (target, key, value) => {

                        Emitter.addListener(key, value, this)
                        target[key] = value
                        return true;
                    },
                    deleteProperty: (target, key) => {
                        Emitter.removeListener(key, this.$options.sockets2[key], this)
                        delete target.key;
                        return true
                    }
                })

                if (sockets) {
                    Object.keys(sockets).forEach((key) => {

                        this.$options.sockets[key] = sockets[key];
                    });
                }
                if (sockets2) {
                    Object.keys(sockets2).forEach((key) => {
                        this.$options.sockets2[key] = sockets2[key];
                    });
                }
            },
            beforeDestroy() {
                let sockets = this.$options['sockets']

                if (sockets) {
                    Object.keys(sockets).forEach((key) => {
                        delete this.$options.sockets[key]
                    });
                }

                let sockets2 = this.$options['sockets2']

                if (sockets2) {
                    Object.keys(sockets2).forEach((key) => {
                        delete this.$options.sockets2[key]
                    });
                }
            }
        })

    }

}