import Vue from 'vue'
import axios from 'axios'
import querystring from 'querystring'
import App from './App'
import router from './router'
import store from './store'
import '!style-loader!css-loader!vue-simple-suggest/dist/styles.css' // Using a css-loader
import VueSimpleSuggest from 'vue-simple-suggest'
Vue.component('vue-simple-suggest', VueSimpleSuggest)
import mousetrap from 'mousetrap';
Vue.prototype.$mousetrap = mousetrap
axios.defaults.withCredentials = true; //让ajax携带cookie
Vue.http = Vue.prototype.$http = axios
Vue.prototype.$qs = querystring;
import iView from 'iview';
import '!style-loader!css-loader!iview/dist/styles/iview.css';
Vue.use(iView);
import '!style-loader!css-loader!bootstrap/dist/css/bootstrap.min.css'
import intro from '@/js/intro-vendor.js'
import VueIntro from 'vue-introjs'
Vue.use(VueIntro, {
  'prevLabel': '&larr; 上一步',
  'nextLabel': '下一步 &rarr;',
  'skipLabel': '跳过'
})
import 'intro.js/introjs.css';
import '!vue-style-loader!css-loader!sass-loader!@/scss/main.scss'
import VueContextMenu from 'vue-contextmenu'
import '!style-loader!css-loader!vue-contextmenu/style/css/font-awesome.min.css'
Vue.use(VueContextMenu);
import '@/js/extend.js';
import $ from '@/js/jquery-vendor.js'
import 'bootstrap'
import '@/js/dark.js'
import {
  app
} from '@/js/app.js'

let pg = require('../../package.json');
app.config = Object.assign(CONFIG, pg);
import VueSocketio from '@/mixins/vue-socket/Main.js';
import socketio from '@/js/socket.io.js';



// import socketio from '@/js/socket.io-client/index.js';

// import socketio from 'socket.io-client'
Vue.prototype.$websock = null;





app.env = process.env || {};

console.log('app.env:::', app.env);
let fs = null,
  readline = null,
  chokidar = null,
  logger = {};
if (!process.env.IS_WEB) {
  Vue.use(require('vue-electron'))
  document.body.classList.add('y-plus');
  app.env.isPlus = true;
  fs = require("fs")
  readline = require('readline');
  chokidar = require("chokidar");
  if (process.platform == 'darwin') {
    document.body.classList.add('y-iOS');
  }


  // const log4js = require('log4js');
  // // logger.level = 'debug';

  // log4js.configure({
  //   appenders: {
  //     everything: {
  //       type: 'dateFile',
  //       filename: 'log/ws.log',
  //       compress: true,
  //       daysToKeep: 5,
  //       keepFileExt: true
  //     },
  //   },
  //   categories: {
  //     default: {
  //       appenders: ['everything'],
  //       level: 'info'
  //     }
  //   }
  // });

  // logger = log4js.getLogger();
}

Vue.use(VueSocketio, store, socketio(`ws://127.0.0.1/trade/user`, {
  transports: ['websocket'],
  autoConnect: false,
  forceNew: true,
  logger: logger
}), socketio(`ws://127.0.0.1/quote/user`, {
  transports: ['websocket'],
  autoConnect: false,
  forceNew: true,
  logger: logger
}));


Vue.config.productionTip = false

Vue.filter('f-time', function (v) {
  return numeral(v).format('0,0');;
});

// 全局混入
Vue.mixin({
  data() {
    return {
      fs: fs,
      readline: readline,
      chokidar: chokidar,
      config: app.config,
      env: app.env,
      dataType: app.dataTypeOriginal,
      dataTypeAll: app.dataType,
      // numChinese: {
      //   1: '一',
      //   2: '二',
      //   3: '三',
      //   4: '四',
      //   5: '五',
      //   6: '六',
      //   7: '七',
      //   8: '八',
      //   9: '九',
      //   10: '十',
      // }
    }
  },
  mounted: function () {
    this.$nextTick(() => {
      var myOption = this.$options.myOption
      if (myOption) {
        let num = document.querySelectorAll('.i-table').length;
      }

    })
  },
  methods: {
    autoTableSize() {
      var self = this;
      $(".i-table").each((i, e) => {
        let el = $(e);
        self.width[e.dataset.k] = el.parent().width();
        let ch = e.dataset.correctH || 0;
        self.height[e.dataset.k] = el.parent().height() - ch;
        var height = self.height['entrust'];
        if (height) {
          self.height['deal'] = height;
        }
      });
    },
  }
})

new Vue({
  router,
  store,
  render: h => h(App),
}).$mount('#app')