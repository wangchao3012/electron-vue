##自动构建发布步骤

1) 在对应环境进行build打包,可以同时进行客户端版和web版打包
2) 将打包好的文件(win:.exe mac:.dmg)和latest.yml文件复制到nginx下官网file下
3) 将web版打包好的文件复制到web版网站下
4) nginx 需要启动2个网站
    官网访问路径暂定：http://10.25.24.105:58081/
nginx目录如下图

![nginx目录](./resources/img1.png)