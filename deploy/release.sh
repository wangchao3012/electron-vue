#!/bin/bash 
#ci 调用的部署文件
serverList=($SERVER) 

expFile="release_exp"
if [[ -n ${CI_COMMIT_TAG} ]];then
        expFile="release_exp.prod"
elif [[ -e "deploy/${expFile}-${NODE_APP_INSTANCE}.sh" ]];then
        expFile="${expFile}-${NODE_APP_INSTANCE}"
fi

echo "执行文件：${expFile}"

for server in ${serverList[@]};
do
    echo "正在发布服务器:${server}..."
    ./deploy/${expFile}.sh $server $USERNAME $PASSWORD
    echo "发布成功：${server}"
done

echo -e "\033[1;32m 全部服务器发布成功：${SERVER} \033[0m"
echo -e "\033[1;33m 😊😊😊 \033[0m"