#!/usr/bin/expect
# server,username,password

set server [lindex $argv 0]
set username [lindex $argv 1]
set password [lindex $argv 2] 

set nginxDir "/usr/share/nginx/xtp/prod"

set timeout 30
spawn ssh $username@$server 

expect {
 "(yes/no)?"
  {
  send "yes\r"
  expect "*assword:*" { send "$password\r"}
 }
 "*assword:*"
    {
    send "$password\r"
    }
}
expect "$*"
send "cd $nginxDir\r"

expect "$*"
send "unzip -q -o XTP.Smart.zip -d temp\r"

expect "$*"
send "rm -rf web website\r"

expect "$*"
send "mv -f temp/* .\r"

expect "$*"
send "rm -rf temp\r"

expect "$*"
send "exit\r"
expect eof