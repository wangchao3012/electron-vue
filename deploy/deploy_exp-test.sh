#!/usr/bin/expect
# server,username,password,CURRENT_DIR_HOME

set server [lindex $argv 0]
set username [lindex $argv 1]
set password [lindex $argv 2] 
# set CURRENT_DIR_HOME [lindex $argv 3] 

set timeout 60
#spawn ssh $username@$server 
# https://www.cnblogs.com/webnote/p/5877920.html
#spawn scp -p -r website web $username@$server:/usr/share/nginx/xtp/
spawn scp -p XTP.Smart.zip $username@$server:/usr/share/nginx/xtp/test/

expect {
 "(yes/no)?"
  {
  send "yes\n"
  expect "*assword:*" { send "$password\n"}
 }
 "*assword:*"
    {
    send "$password\n"
    }
}
expect "*100%*"
send "exit\r"
expect eof