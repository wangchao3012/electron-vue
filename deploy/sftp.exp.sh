#!/usr/bin/expect

set sftp_ip [lindex $argv 0]
set sftp_name [lindex $argv 1]
set sftp_pwassword [lindex $argv 2]
set sftp_prot [lindex $argv 3] 
set timeout 300
spawn sftp -P $sftp_prot smart@$sftp_ip
expect {
 "(yes/no)?"
  {
  send "yes\r"
  expect "*assword:*" { send "$sftp_pwassword\r"}
 }
 "*assword:*"
    {
    send "$sftp_pwassword\r"
    }
}

expect "sftp>"
# send "lcd deploy\r"
# 由于网络传输时间关系，版本描述文件文件yml如果提前上传完成，更新程序没有上传完成，会导致客户端更新时无法正确下载完整正确的更新文件，所有要有上传顺序

# XTP.Smart.exe
# latest.yml
# XTP.Smart.dmg
# XTP.Smart.zip 
# latest-mac.yml

expect "sftp>"
send "put XTP.Smart.exe XTP.Smart.exe\r"

expect "sftp>"
send "put latest.yml latest.yml\r"

expect "sftp>"
send "put XTP.Smart.dmg XTP.Smart.dmg\r"

expect "sftp>"
send "put XTP.Smart.zip XTP.Smart.zip\r"

expect "sftp>"
send "put latest-mac.yml latest-mac.yml\r"

expect "sftp>"
send "ls -l\r"

expect "sftp>"
send "exit\r"
expect eof