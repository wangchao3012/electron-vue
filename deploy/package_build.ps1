chcp 65001 | Out-Null
$env:path += ";C:\Users\Administrator\AppData\Roaming\npm"
  
write-host "begin to build xtp_rich_client..."
$env:NODE_APP_INSTANCE=$env:CI_COMMIT_REF_NAME.Substring($env:CI_COMMIT_REF_NAME.LastIndexOf("-")+1)

write-host $env:NODE_APP_INSTANCE
(npm-cache install) -and (npm run build)
$returnCode = $LASTEXITCODE
if($returnCode -eq 0 ) {
   mv build\*.exe .\
   mv build\latest.yml .\
   write-host "xtp_rich_client successful builded."
} else {
  write-host "fail to build xtp_rich_client."
  exit ( $returnCode ) 
}


