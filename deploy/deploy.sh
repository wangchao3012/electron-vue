#!/bin/bash 

serverList=($SERVER) 
mv -f *.zip *.dmg latest*.yml *.exe website/dl/smart/
echo "开始压缩文件：XTP.Smart.zip"
zip -q -r XTP.Smart.zip web website
echo "压缩文件成功"

expFile="deploy_exp"
if [[ -n ${CI_COMMIT_TAG} ]];then
        expFile="deploy_exp.prod"
elif [[ -e "deploy/${expFile}-${NODE_APP_INSTANCE}.sh" ]];then
        expFile="${expFile}-${NODE_APP_INSTANCE}"
fi
echo "执行文件：${expFile}"


for server in ${serverList[@]};
do
    echo "正在部署服务器：${server}..."
    ./deploy/${expFile}.sh $server $USERNAME $PASSWORD
    echo "部署成功：${server}"
done

echo -e "\033[1;32m 全部服务器部署成功：${SERVER} \033[0m"
echo -e "\033[1;33m 😊😊😊 \033[0m"