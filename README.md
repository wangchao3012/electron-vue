# xtp_rich_client

[开发版](http://10.25.24.72:800)

[测试版](http://10.25.24.72:801)

[正式版](https://xtp.zts.com.cn/download/client)

## 常见问题
※为了顺利的安装npm包，请将下载npm源路径设置为淘宝镜像地址 

> 执行此命令：
  ``` bash
  1) npm config set registry https://registry.npm.taobao.org
  2) npm config get registry (验证registry是否配置成功)
  3) npm config set disturl https://npm.taobao.org/dist
  4) npm config get disturl (验证disturl是否配置成功)
  ```

1.安装报错（运行时报sass相关错误）

  解决办法
  > 执行此命令安装： 
    ``` bash
       npm install phantomjs-prebuilt@2.1.14 --ignore-scripts
    ```

2.错误：Module build failed: Error: ENOENT: no such file or directory, scandir 'E:\gitlab\admin\node_modules
  
  解决办法
  > 执行安装命令：npm rebuild node-sass

3.错误：ChromeDriver installation failed Error with
  
  解决办法
  >执行安装命令：npm install chromedriver --chromedriver_cdnurl=http://npm.taobao.org/mirrors/chromedriver
  
  npm install electron -g --registry=http://registry.npm.taobao.org

Windows上的Git默认是大小写不敏感的，这样多平台协作或build就可能会出现问题

将Win上的Git设置为大小写敏感的命令如下
> 执行安装命令：git config core.ignorecase false
                 
第一次启动，依次执行：

1) npm i
2) npm run dev

登录服务器完整示例：https://smart.qilu-dev.com:443

### 常用vscode插件

> Beautify: javascript,JSON, CSS, Sass,HTML 代码提示、格式化

> Bracket Pair Colorizer: 不同层级括号匹配

> Bookmarks: 书签工具

> Local History:文档修改历史记录

> vscode-icons: 文件夹图标

### 修改默认设置

> "editor.formatOnSave": true,

当前build node版本是v8.11.1